#ifndef INCLUDED_DECRYPTOR_
#define INCLUDED_DECRYPTOR_

#include <string>
#include <fstream>
#include <memory>

class Options;
class GPGHandler;
class PGPSection;

#include <bobcat/tempstream>

class Decryptor
{
    Options const &d_options;
    GPGHandler &d_gpgHandler;

    std::unique_ptr<std::ofstream> d_fverbose;
    std::unique_ptr<FBB::TempStream> d_pgpMessage;
    std::unique_ptr<FBB::TempStream> d_decrypted;
    std::string d_decryptedName;

    public:
        Decryptor(GPGHandler &gpgHandler);
                                                // -----BEGIN PGP MESSAGE-----
                                                // was just read by
        void process(std::istream &in);         // Guncat::process

    private:
        void clearFiles() const;
        void decrypt(PGPSection const &pgpSection) const;

        void setVerbose();

        static void error(int ret, std::string const &msg);
};

#endif





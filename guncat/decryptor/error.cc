#include "decryptor.ih"

// static
void Decryptor::error(int ret, string const &msg)
{
    throw Exception{} << msg << ", gpg returns " << ret;
}

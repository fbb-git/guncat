#define XERR
#include "gpghandler.ih"

    // decryptor/process.cc allows repeated attempts
int GPGHandler::process(string const &filename)
{
    d_filename = filename;
  xerr("STARTS " << d_gpgCommand << " for "  << d_filename);

    resetPipe();
    fork();

  xerr("LEAVING with " << d_ret);
    return d_ret;           // this section is now completed
}



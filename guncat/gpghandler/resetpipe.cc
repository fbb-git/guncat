//#define XERR
#include "gpghandler.ih"

void GPGHandler::resetPipe()
{
    d_childMessages.close();
    d_childMessages = Pipe{};
}

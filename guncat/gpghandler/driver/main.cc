#include "main.ih"

namespace
{
    string terminating{ "Terminating" };
}

int main(int argc, char **argv)
try
{
//    if (argc == 1)
//    {
//        cerr << "file containing PGP SECTION required\n";
//        return 1;
//    }

    Arg const &arg = prepareArgs(argc, argv);
    (terminating += ' ') += arg.basename();

    Options const &options = Options::instance();

    ifstream in(arg[0]);

//    string passwd;

//    if (options.getPassphrase())
//        getline(in, passwd);

    GPGHandler handler(options.passphraseFirstLine());
    handler.checkPassphrase(cin);

//    handler.process(in);

}
catch (int x)
{
    cerr << "returning " << x << '\n';
    return 0;
}
catch (exception const &exc)
{
    cerr << terminating << ": " << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "Unexpected exception\n";
    return 1;
}

#include "options.ih"

void Options::extraGPGoptions()
{
    if (d_pgpRanges)
        return;
    
    if (string option; size_t nOptions = d_arg.option(&option, "gpg-option"))
    {
        (d_gpgDecrypt += ' ') += option;

        for (size_t idx = 1; idx != nOptions; ++idx)
        {
            d_arg.option(idx, &option, "gpg-option");
            (d_gpgDecrypt += ' ') += option;
        }
    }
}

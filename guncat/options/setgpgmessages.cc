//#define XERR
#include "options.ih"

void Options::setGpgMessages()
{
    string filename;

    if (not d_arg.option(&filename, 'm'))
        d_gpgMessages = 0;        
    else if (filename == "stdout")
        d_gpgMessages = &cout;
    else if (filename == "stderr")
        d_gpgMessages = &cerr;
    else
    {
        d_msgPtr.reset(
                    new ofstream{ Exception::factory<ofstream>(filename) }
                 );
        d_gpgMessages = d_msgPtr.get();
    }
}

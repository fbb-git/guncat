//                     usage.cc

#include "main.ih"

namespace
{
    char const info[] = R"R([options] [file(s)]
   Where:
   [options] - optional arguments (short options between parentheses):

      --dots (-d): write a dot (.) to the standard error stream after
                   processing each block of 8192 input lines. This option
                   is ignored when the --pgp-ranges, --section-lines, or
                   --verbose options are specified. 
      --gpg-command: show the gpg command that would be used, and quit.
      --gpg-path path: path to the gpg program (default: /usr/bin/gpg).
      --gpg-option "spec": option `spec' is passed to gpg child processes
                  (multiple --gpg-option options may be specified).
      --help (-h): provide this help (ends guncat, returning 0).
      --no-errors: processing ends if an invalid PGP section is encountered or
                  if gpg returns a non-zero exit value. Otherwise processing
                  continues beyond the failing PGP section, reporting the line
                  numbers of the failing PGP section.
      --passphrase path: the passphrase is read as the first line from `path'.
      --pgp-messages (-m) path to the file receiving the GPG message. Use
                  `stdout' to write the messages to the standard output
                  stream, use `stderr' to write the messages to the standard
                  error stream
      --pgp-ranges (-r): the lines-ranges of complete PGP MESSAGE
                  sections are reported. No additional output is produced.
      --quoted-printable (-q): merely decrypt PGP messages, keeping their
                  quoted-printable content (by default quoted-printable
                  content like '=3D' is converted to ascii).
      --reduce-headers (-R): only output the mail headers Cc:, Date:, From:, 
                  Subject:, and To:.
      --section-lines (-S): in the output precede decrypted PGP sections by
                  their line numbers.
      --skip-incomplete (-s): incomple PGP MESSAGE sections are not outputted.
      --time-limit (-T) <seconds>: maximum allowed time in seconds for
                  decrypting an encrypted section (by default: no time limit
                  is used).
      --tty-OK (-t): by default gpg's option --no-tty is used.
      --verbose (-V) - write extra messages to stderr. Use `--verbose=path' or
                  `-Vpath' to write extra messages to the file `path'.
      --version (-v)    - show version information (ends guncat, returning 0).
      --write (-w) path - path of the file to receive the output. By default
                  cout is used.

   [file(s)] - file(s) to process. Standard input redirection can also be
                  used. By default the processed information is written to the
                  standard output stream, but may also be written to file
                  (using --write or standard output redirection), or may be
                  sent to a the standard input stream of another program
                  (using --less to use /usr/bin/less or --pipe to specify
                  another program).

)R";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;
}


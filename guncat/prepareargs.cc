#include <bobcat/arg>

using namespace std;
using namespace FBB;

// initializing FBB::Arg is separated from main() so it can be used by
// the various driver programs: they declare 
//
//      Arg const &prepareArgs(int argc, char **argv);

namespace
{
    Arg::LongOption longOptions[] =
    {
        Arg::LongOption("dots",             'd'),
        Arg::LongOption("gpg-command",      Arg::NoArg),
        Arg::LongOption("gpg-option",       Arg::Required),
        Arg::LongOption("gpg-path",         Arg::Required),
        Arg::LongOption("help",             'h'),
        Arg::LongOption("no-errors",        Arg::NoArg),
        Arg::LongOption("passphrase",       Arg::Required),
        Arg::LongOption("pgp-messages",     'm'),
        Arg::LongOption("pgp-ranges",       'r'),
        Arg::LongOption("quoted-printable", 'q'), 
        Arg::LongOption("reduce-headers",   'R'), 
        Arg::LongOption("section-lines",    'S'), 
        Arg::LongOption("skip-incomplete",  's'), 
        Arg::LongOption("time-limit",       'T'),
        Arg::LongOption("tty-OK",           't'),
        Arg::LongOption("verbose",          'V'),   // optional argument
        Arg::LongOption("version",          'v'),
        Arg::LongOption("write",            'w'),
    };
}

Arg const &prepareArgs(int argc, char **argv)
{
    return Arg::initialize("dhm:qRrsStT:vV::w:",
                           longOptions, end(longOptions), argc, argv);
}

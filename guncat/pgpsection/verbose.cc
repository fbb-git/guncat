//#define XERR
#include "pgpsection.ih"

void PGPSection::verbose(string const &what) const
{
    if (g_verbose != 0)
        *g_verbose << g_filename << ':' << d_firstLine << ':' << g_lineNr << 
                    ": PGP MESSAGE: " << what << '\n';
}

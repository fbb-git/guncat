//#define XERR
#include "pgpsection.ih"

    // in: current input file, at 'PGP MESSAGE'
PGPSection::PGPSection(istream &in, TempStream &pgpMessage)
:
    d_pgpMessage(pgpMessage),
    d_in(in),
    d_entryOffset(in.tellg()),
    d_offset(d_entryOffset)
{
    Options const &options = Options::instance();
    d_pgpRanges = options.pgpRanges() | options.sectionLines();
}

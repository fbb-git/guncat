//#define XERR
#include "pgpsection.ih"

bool PGPSection::nextLine()
{
    if (not ::nextline(d_in, d_line))
        return false;

    d_pgpMessage << d_line << '\n';
    return true;
}

#include "guncat.ih"

void Guncat::showGPGcommand() const
{
    cout << d_options.gpgPath() << 
            " --pinentry-mode loopback --passphrase-fd <fd>" <<
            d_options.gpgDecrypt() << '\n';
}

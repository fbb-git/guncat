#include "guncat.ih"

void Guncat::programArguments()
{
    if (onlyCin())
        return;

    Arg &arg = Arg::instance();

    for (size_t idx = 0, end = arg.nArgs(); idx != end; ++idx)
    {
        g_filename = arg[idx];
        ifstream in{ Exception::factory<ifstream>(g_filename) };

        process(in);
    }
}

#include "guncat.ih"

char const *const Guncat::s_accept[] =
{
    "Cc: ", 
    "Date: ", 
    "From: ", 
    "Subject: ", 
    "To: "
};

char const *const *const Guncat::s_acceptEnd = s_accept + size(s_accept);
